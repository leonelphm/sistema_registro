"""sistema_registro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.views.static import serve
from django.conf.urls import include, url
from django.contrib import admin
from users.urls import urlpatterns_users
from utils.urls import urlpatterns_autocomplete
from registros.urls import urlpatterns_registro
from django.contrib.auth.views import (
    password_reset_confirm, password_reset_complete
    )

from users.forms import SetPasswordForm

urlpatterns = [
    url(r'^recursos/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT}),
    url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm,
        {'template_name': 'users/passwordresetconfirm.html',
         'post_reset_redirect': '/user/password/done/',
         'set_password_form': SetPasswordForm},
        name='password_reset_confirm'),
    url(r'^user/password/done/$', password_reset_complete,
        {'template_name': 'users/passwordresetcomplete.html'}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(urlpatterns_users, namespace="users")),
    url(r'^utils/', include(urlpatterns_autocomplete, namespace="utils")),
    url(r'^registrar/', include(urlpatterns_registro, namespace="registro")),
]
