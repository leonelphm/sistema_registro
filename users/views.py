# -*- encoding: utf-8 -*-
"""!
Vista que controla los procesos de los usuarios

@author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
@date 09-01-2017
@version 1.0.0
"""

from django import forms
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (
    authenticate, logout, login
)

from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.auth.models import (
    Group, Permission, User
)
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.mixins import (
    LoginRequiredMixin
)
from django.contrib.contenttypes.models import ContentType
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import (
    reverse_lazy, reverse
)

from django.shortcuts import (
    render, redirect
)
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.edit import (
    FormView, UpdateView
)

from .forms import (
    FormularioLogin, FromularioRegistro, FromularioUpdate
)

from .utils import (
    LoginRequeridoPerAuth
)


class LoginView(FormView):
    """!
    Muestra el formulario de ingreso a la aplicación SERE

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    form_class = FormularioLogin
    template_name = 'users/login.html'
    success_url = '/inicio/'

    def form_valid(self, form):
        """
        Valida el formulario de logeo
        @return: Dirige a la pantalla inicial de la plataforma
        """
        usuario = form.cleaned_data['usuario']
        contrasena = form.cleaned_data['contrasena']
        usuario = authenticate(username=usuario, password=contrasena)
        if usuario is not None:
            login(self.request, usuario)
            self.request.session['permisos'] = list(usuario.get_all_permissions())
            try:
                self.request.session['grupos'] = list(usuario.groups.get())
            except:
                grupo = str(usuario.groups.get())
                self.request.session['grupos'] = grupo
            if self.request.POST.get('remember_me') is not None:
                # Session expira a los dos meses si no se deslogea
                self.request.session.set_expiry(1209600)
            messages.info(self.request, 'Bienvenido %s has ingresado a el \
                                         Sistema de registor de eventos con el usuario %s \
                                         ' % (usuario.first_name,
                                              usuario.username))
        else:
            user = User.objects.filter(username=form.cleaned_data['usuario'])
            if user:
                user = user.get()
                if not user.is_active:
                    self.success_url = reverse_lazy('users:login')
                    messages.error(self.request, 'La cuenta esta inactiva \
                                                consulte con un adminitrador')
                else:
                    self.success_url = reverse_lazy('users:login')
                    messages.warning(self.request, 'Verifique su nombre y contraseña\
                                                 y vuelve a intertar')

        return super(LoginView, self).form_valid(form)


class LogOutView(RedirectView):
    """!
    Salir de la apliacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    permanent = False
    query_string = True

    def get_redirect_url(self):
        """!
        Dirige a la pantalla del login
        @return: A la url del login
        """
        logout(self.request)
        return reverse_lazy('users:login')


class ListUsersView(LoginRequeridoPerAuth, TemplateView):
    """!
    Listar usuarios de la apliacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "users/user_list.html"
    model = User
    success_url = reverse_lazy('users:lista_users')
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListUsersView, self).__init__()


    def post(self, *args, **kwargs):
        '''
        Cambia el estado activo a el usuario
        @return: Dirige a la tabla que muestra los usuarios de la apliacion
        '''
        accion = self.request.POST
        activar = accion.get('activar', None)
        inactivar = accion.get('inactivar', None)
        estado = False

        if activar is not None:
            user = activar
            estado = True
        elif inactivar is not None:
            user = inactivar
            estado = False
        else:
            messages.error(self.request, "Esta intentando hacer una accion incorrecta")      
        try:
            user_act = self.model.objects.get(pk=user)
            user_act.is_active = estado
            user_act.save()
            if estado:
                messages.success(self.request, "Se ha activado el usuario: %s" % (str(user_act)))
            else:
                messages.warning(self.request, "Se ha inactivado el usuario: %s" % (str(user_act)))
        except:
            messages.info(self.request, "El usuario no existe")
        return redirect(self.success_url)


class ListUsersJsonView(LoginRequeridoPerAuth, BaseDatatableView):
    """!
    Prepara la data para mostrar en el datatable

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    # The model we're going to show
    model = User
    # define the columns that will be returned
    columns = ['pk', 'first_name', 'last_name', 'username', 'email',
               'date_joined', 'last_joined', 'is_active', 'groups']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['pk', 'username', 'first_name', 'last_name', 'email',
                     'date_joined', 'last_login', 'is_active', 'groups']
    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListUsersJsonView, self).__init__()

    def get_initial_queryset(self):
        """!
        Consulta el modelo User

        @return: Objeto de la consulta
        """
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return self.model.objects.all()

    def prepare_results(self, qs):
        """!
        Prepara la data para mostrar en el datatable
        @return: Objeto json con los datos de los usuarios
        """
        # prepare list with output column data
        json_data = []
        for item in qs:
            user = "<a data-toggle='modal' data-target='#myModal' class='btn btn-block btn-info btn-xs fa fa-edit' onclick='modal_user(%s)'>%s</a>" % (str(item.pk), str(item.username))
            if item.last_login:
                last_login = item.last_login.strftime("%Y-%m-%d %H:%M:%S")
            else:
                last_login = "No ha ingresado"
            if item.is_active:
                activo = "Activo"
                activar = "<input type='checkbox' id='user-" + str(item.pk) + "' value='" + str(item.pk) + "' name='inactivar' onclick='$(\"#forma_activar\").submit();'/>\
                            <label for='user-" + str(item.pk) + "'>\
                                <img  src='" + settings.MEDIA_URL + \
                                "imagenes/inactivar.png' id='inactivo' \
                                title='Inactivar Usuario' \
                                />\
                            </label>"
                
            else:
                activo = "Inactivo"
                activar = "<input type='checkbox' id='user-" + str(item.pk) + "' value='" + str(item.pk) + "' name='activar' onclick='$(\"#forma_activar\").submit();'/>\
                            <label for='user-" + str(item.pk) + "'>\
                                <img  src='" + settings.MEDIA_URL + \
                                "imagenes/activar.png' id='activo' \
                                title='Activar Usuario'/>\
                            </label>"
            try:
                grupo = str(item.groups.get())
            except:
                grupo = "No pertenece a un grupo"  
            json_data.append([
            user,
            "{0} {1}".format(str(item.first_name), str(item.last_name)),
            item.email,
            item.date_joined.strftime("%Y-%m-%d %H:%M:%S"),
            last_login,
            activo,
            grupo,
            activar
            ])
        return json_data


class StartView(LoginRequiredMixin, TemplateView):
    """!
    Muestra el inicio de la plataforma

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    @return: El template inicial de la plataforma
    """
    template_name = "starter.html"

    def dispatch(self, request, *args, **kwargs):
        """
        Envia una alerta al usuario que intenta acceder sin estar logeado
        @return: Direcciona al login en caso de no poseer permisos, en caso contrario usa la clase
        """
        if not request.user.is_authenticated:
            messages.warning(self.request, "Debes iniciar Sessón")
            return self.handle_no_permission()
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

    def __init__(self):
        super(StartView, self).__init__()


class RegisterView(LoginRequeridoPerAuth, FormView):
    """!
    Muestra el formulario de registro de usuarios

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "users/register.html"
    form_class = FromularioRegistro
    success_url = reverse_lazy('users:home')
    model = Group
    model_permi = Permission
    permission_required = 'auth.add_user'

    def get_context_data(self, **kwargs):
        """
        Carga el formulario en la vista,para registrar usuarios
        @return: El contexto con los objectos para la vista
        """
        return super(RegisterView, self).get_context_data(**kwargs)

    def form_valid(self, form, **kwargs):
        """
        Valida el formulario de registro de usuarios
        @return: Dirige con un mensaje de exito a el home
        """
        formulario = self.form_class(self.request.POST)
        nuevo_usuario = formulario.save()
        usuario = form.cleaned_data['username']
        grupos = form.cleaned_data['groups']
        permiso = form.cleaned_data['user_permissions']
        for group in form.cleaned_data['groups']:
            add_groupo = self.model.objects.get(pk=group.pk)
            # Agrega a el usuario al(los) grupo(s) seleccionado(s)
            nuevo_usuario.groups.add(group.pk)
        for permisos in form.cleaned_data['user_permissions']:
            add_permisos = self.model_permi.objects.get(pk=permisos.pk)
            # Le Agrega el(los) permiso(s) seleccionados a el usuario
            nuevo_usuario.user_permissions.add(permisos.pk)
        model_user = ContentType.objects.get_for_model(User).pk
        LogEntry.objects.log_action(
            user_id=self.request.user.id,
            content_type_id=model_user,
            object_id=nuevo_usuario.id,
            object_repr=str(nuevo_usuario.username),
            action_flag=ADDITION)
        messages.success(self.request, "Usuario %s creado con exito\
                                       " % (str(usuario)))
        return super(RegisterView, self).form_valid(form)


class CargarPermisosAjax(LoginRequeridoPerAuth, FormView):
    """!
    Construye el ajax para cargar los permisos

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "users/permisos.html"
    form_class = FromularioRegistro
    permission_required = 'auth.add_user'

    def get_context_data(self, **kwargs):
        """!
        Carga los permisos dado el grupo
        @return: El objeto form restructurado con los choices
        """
        form = self.form_class()
        permisos = []
        choice = []
        grupos = self.request.GET
        val_group = dict(grupos.lists())
        if val_group['groups[]']:
            for val in val_group['groups[]']:
                for elem in val:
                    permisos += Permission.objects.filter(
                                group=elem
                                ).values('pk', 'name'),
            for permiso in permisos:
                for e in permiso:
                    choice += (e['pk'], e['name']),
        else:
            pass
        form.fields['user_permissions'].widget = forms.SelectMultiple(choices=choice)
        context = super(CargarPermisosAjax, self).get_context_data(**kwargs)
        context['form'] = form
        return context


class ModalsPerfil(LoginRequiredMixin, UpdateView):
    """!
    Construye el modals para la actualizacion del usuario

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    model = User
    form_class = FromularioUpdate
    template_name = 'users/modal_userPerfil.html'
    success_url = reverse_lazy('users:home')
    success_message = 'Usuario Actualizado con exito'

    def dispatch(self, request, *args, **kwargs):
        """
        Envia una alerta al usuario que intenta acceder sin estar logeado
        @return: Direcciona al login en caso de no poseer permisos, en caso contrario usa la clase
        """
        if not request.user.is_authenticated:
            messages.warning(self.request, "Debes iniciar Sessón")
            return self.handle_no_permission()
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class NotFoundView(TemplateView):
    """!
    Dirige a la plantilla de pagina no funciona

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "404.html"


class Forbidden(TemplateView):
    """!
    Dirige a la plantilla en la que no tiene permiso

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>
    GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "403.html"


class ErrorServerView(TemplateView):
    """!
    Dirige a la plantilla de que existe un error en el servidor

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>
    GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "500.html"


class RecentActivityAjaxView(LoginRequiredMixin, TemplateView):
    """docstring for ClassName"""
    template_name = "users/recent_activity.html"
    model = LogEntry

    def get_context_data(self, **kwargs):
        """!
        Carga las actividades recientes del usuario
        @return: El objeto con las actividades recientes del usuario
        """
        user = self.request.GET.get('user')
        log = self.model.objects.select_related().all().order_by("id")
        context = super(RecentActivityAjaxView, self).get_context_data(**kwargs)
        context['log'] = log
        return context
