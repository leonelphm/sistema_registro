# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import (
    password_reset, password_reset_done
)
from .views import *
from .forms import PasswordResetForm

urlpatterns_users = [
    url(r'^$', LoginView.as_view(), name="login"),
    url(r'^accounts/password/reset/$', password_reset,
        {'post_reset_redirect': '/accounts/password/done/',
         'template_name': 'users/forget.html',
         'password_reset_form': PasswordResetForm}, name="forget"),
    url(r'^accounts/password/done/$', password_reset_done,
        {'template_name': 'users/passwordresetdone.html'},
        name='reset_done'),
    url(r'^logout/$', LogOutView.as_view(), name="logout"),
    url(r'^inicio/$', StartView.as_view(), name="home"),
    url(r'^registrar/$', RegisterView.as_view(), name="registrar"),
    url(r'^lista-usuarios/$', ListUsersView.as_view(), name="lista_users"),
    url(r'^listar-usuarios/$', ListUsersJsonView.as_view(),
        name="listar_users"),
    url(r'^cargar-permisos/$', CargarPermisosAjax.as_view(),
        name="cargar_permisos"),
    url(r'^perfil/(?P<pk>\d+)/$', ModalsPerfil.as_view(),
        name="modal_perfil"),
    url(r'^recent-activity/$', RecentActivityAjaxView.as_view(),
        name="recent_activity"),
    url(r'^403/$', Forbidden.as_view(), name="403error"),
    url(r'^404/$', NotFoundView.as_view(), name="404error"),
    url(r'^500/$', ErrorServerView.as_view(), name="500error"),
]
