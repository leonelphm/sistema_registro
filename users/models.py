from django.db import models
from django.contrib.auth.models import Group

"""
Se agrega un campo de descripcion al modelo group para describir el grupo de usuarios
"""
Group.add_to_class('descripcion', models.TextField(blank=True))
