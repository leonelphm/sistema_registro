# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import (
    PermissionRequiredMixin, LoginRequiredMixin
)
from django.shortcuts import (
    redirect
)


class LoginRequeridoPerAuth(LoginRequiredMixin, PermissionRequiredMixin):

    def dispatch(self, request, *args, **kwargs):
        """
        Envia una alerta al usuario que intenta acceder sin permisos para esta clase
        @return: Direcciona al login en caso de no poseer permisos, en caso contrario usa la clase
        """
        if not request.user.is_authenticated:
            messages.warning(self.request, "Debes iniciar Sessón")
            return self.handle_no_permission()
        if not self.has_permission():
            return redirect('users:403error')
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)