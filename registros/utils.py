import re
from .models import *
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

def obtenerModalidad():
    """
    Función que permite obtener la lista de la modalidad de participacion

    El método hace una lista consultando el modelo de modalidad

    @return: Lista de modalidad
    """
    try:
        if ModalidadParticipacion.DoesNotExist:
            consulta = ModalidadParticipacion.objects.all().filter(activo=True).values('pk', 'opcion_modalidad')
        else:
            consulta = [{'pk': '', 'opcion_modalidad': ''}]
    except :
        consulta = [{'pk': '', 'opcion_modalidad': ''}]

    return consulta


def obtenerEvento():
    """
    Función que permite obtener la lista de los eventos activos registrados

    El método hace una lista consultando el modelo de modalidad

    @return: Lista de modalidad
    """
    try:
        if Evento.DoesNotExist:
            consulta = Evento.objects.all().filter(activo=True).values('pk', 'nombre_evento')
        else:
            consulta = [{'pk': '', 'nombre_evento': ''}]
    except :
        consulta = [{'pk': '', 'nombre_evento': ''}]

    return consulta

def obtenerAllEvento():
    """
    Función que permite obtener la lista de los eventos activos registrados

    El método hace una lista consultando el modelo de modalidad

    @return: Lista de modalidad
    """
    try:
        if Evento.DoesNotExist:
            consulta = Evento.objects.all().values('pk', 'nombre_evento')
        else:
            consulta = [{'pk': '', 'nombre_evento': ''}]
    except :
        consulta = [{'pk': '', 'nombre_evento': ''}]

    return consulta


def validarString(cadena):
    """
    Funcion que permite validar una cadena
    @param: Cadena se recibe el parametro cadena para la evaluacion
    @return: True or False
    """
    cadena = cadena.lower()
    if not(type(cadena) == str and not(re.search('\d', cadena))):
        return True
    else:
        return False

def validarDigit(cadena):
    """
    Funcion que permite validar una cadena
    @param: Cadena se recibe el parametro cadena para la evaluacion
    @return: True or False
    """

    if not(str(cadena).isdigit()):
        return True
    else:
        return False

def validarModalidad(cadena):
    """
    Funcion que permite validar una cadena
    @param: Cadena se recibe el parametro cadena para la evaluacion
    @return: True or False
    """
    tipo_modalidad = ['asistente', 'ponente', 'organizador']
    if re.search('/', cadena):
        cadena = cadena.split("/")
        for cad in cadena:
            if not((type(cad) == str and not(re.search('\d', cad)) and
                cad.lower() in tipo_modalidad)):
                return True
    else:
        if not((type(cadena) == str and not(re.search('\d', cadena)) and
                    cadena.lower() in tipo_modalidad)):
            return True
        else:
            return False


def validarCorreo(cadena):
    """
    Funcion que permite validar una cadena
    @param: Cadena se recibe el parametro cadena para la evaluacion
    @return: True or False
    """
    try:
        validate_email(cadena)
        return True
    except ValidationError:
        return False
