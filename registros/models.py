import pyexcel
import re

from django.db import models



class Evento(models.Model):
    """!
    Clase que contiene los eventos realizados

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18-04-2017
    @version 1.0.0
    """
    def get_upload_to(self, filename):
        return "certificados/%s/%s" % (self.nombre_evento, filename)

    nombre_evento = models.CharField(max_length=128, unique=True)
    fecha_inicio = models.DateField(null=False)
    fecha_fin = models.DateField(null=False)
    certificado = models.FileField(upload_to=get_upload_to)
    activo = models.BooleanField(default=True)

    def __str__(self):
        """!
        Fucncion que muestra el evento
        @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
        @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
        @date 18-04-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Devuelve el identificador del evento
        """
        return str(self.nombre_evento)


class ModalidadParticipacion(models.Model):
    """!
    Clase que contiene las opciones de la Modalidad para la participacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18-04-2017
    @version 1.0.0
    """
    opcion_modalidad = models.CharField(max_length=128)
    activo = models.BooleanField(default=True)

    def __str__(self):
        """!
        Fucncion que muestra la opcion de la modalidad en la que participa
        @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
        @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
        @date 18-04-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Devuelve el identificador de la opcion
        """
        return str(self.opcion_modalidad)


class Participante(models.Model):
    """!
    Clase que contiene los datos de los participantes

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18-04-2017
    @version 1.0.0
    """
    nombres = models.CharField(max_length=128)
    apellidos = models.CharField(max_length=128)
    cedula = models.IntegerField(unique=True)
    correo = models.EmailField(max_length=78, null=True)
    telefono = models.IntegerField(null=True)
    organizacion = models.CharField(max_length=128)

    class Meta:
        ordering = ('cedula',)

    def __str__(self):
        """!
        Fucncion que muestra la opcion de la modalidad en la que participa
        @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
        @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
        @date 18-04-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Devuelve el identificador de la opcion
        """
        return str(self.cedula)+" "+str(self.nombres)+" "+str(self.apellidos)


    def uploadCreateUpdate(self, file, user, extension, evento, *args, **kwargs):
        load_file = pyexcel.get_sheet(file_name=file)
        result = True
        message = True
        tipo_modalidad = ['asistente', 'ponente', 'organizador']
        if extension == "csv":
            csv = True
        else:
            csv = False
        for row in load_file.row[1:]:
            if csv:
                row = row[0].split(';')
            row_nombres = row[0].title().strip()
            row_apellidos = row[1].title().strip()
            row_ci = row[2]
            row_correo = row[3].strip()
            row_telefono = row[4]
            row_modalidad = row[5].title().strip()
            row_organizacion = row[6].upper().strip()

            objeto_parCrUp, creado = Participante.objects.update_or_create(
                    cedula=row_ci,
                    defaults={'nombres': row_nombres,
                              'apellidos': row_apellidos, 'correo': row_correo,
                              'telefono': row_telefono,
                              'organizacion': row_organizacion}
                )

            if re.search('/', row_modalidad):
                row_modalidad = row_modalidad.split("/")
                for cad in row_modalidad:
                    modalidad = ModalidadParticipacion.objects.get(opcion_modalidad=cad)
                    objeto_parEvent = ParticipanteEvento.objects.update_or_create(
                                fk_evento = evento, fk_participante = objeto_parCrUp, fk_modalidad = modalidad,
                                defaults={'fk_modalidad': modalidad})
            else:
                modalidad = ModalidadParticipacion.objects.get(opcion_modalidad=row_modalidad)
                objeto_parEvent = ParticipanteEvento.objects.update_or_create(
                                fk_evento = evento, fk_participante= objeto_parCrUp,
                                defaults={'fk_modalidad': modalidad})

        return {'result': result, 'message': message}


class ParticipanteEvento(models.Model):
    """!
    Clase que contiene los datos que relaciona un usuario al evento

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18-04-2017
    @version 1.0.0
    """
    def get_upload_to(self, filename):
        print (self.fk_evento)
        return "certificados/%s/%s" % (self.fk_evento, filename)

    fk_participante = models.ForeignKey(Participante)
    fk_evento = models.ForeignKey(Evento)
    fk_modalidad = models.ForeignKey(ModalidadParticipacion)
    certificado = models.FileField(upload_to=get_upload_to)

    class Meta:
        unique_together = (('fk_evento', 'fk_modalidad', 'fk_participante'),)

    def __str__(self):
        """!
        Fucncion que muestra la cedula del participante
        @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
        @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
        @date 18-04-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @return Devuelve el identificador de la opcion
        """
        return str(self.fk_participante)
