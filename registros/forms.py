
import pyexcel
import os

from dal import autocomplete
from django.conf import settings
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django import forms
from .models import *
from .utils import (obtenerModalidad, obtenerEvento, validarString,
    validarDigit, validarModalidad, validarCorreo, obtenerAllEvento)


class ParticiapanteForm(forms.ModelForm):
    fk_modalidad = forms.CharField()
    fk_evento = forms.CharField()

    class Meta:
        model = Participante
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
            modalida = obtenerModalidad()
            eventos = obtenerEvento()
            super(ParticiapanteForm, self).__init__(*args, **kwargs)
            self.fields['fk_modalidad'].widget = forms.Select(choices=[("", "Seleccione Modalidad")] +
            [(mod["pk"], mod["opcion_modalidad"]) for mod in modalida])
            self.fields['fk_modalidad'].widget.attrs.update({'class': 'form-control'})
            self.fields['fk_modalidad'].required=True
            self.fields['fk_modalidad'].label='Modalidad en la que participa'
            self.fields['fk_evento'].widget = forms.Select(choices=[("", "Seleccione Evento")] +
            [(even["pk"], even["nombre_evento"]) for even in eventos])
            self.fields['fk_evento'].widget.attrs.update({'class': 'form-control'})
            self.fields['fk_evento'].required=True
            self.fields['fk_evento'].label='Evento en el que participa'
            self.fields['nombres'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Nombres'})
            self.fields['nombres'].required=True
            self.fields['apellidos'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Apellidos'})
            self.fields['apellidos'].required=True
            self.fields['cedula'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'cedula'})
            self.fields['cedula'].required=True
            self.fields['correo'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Correo'})
            self.fields['cedula'].required=False
            self.fields['telefono'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Telefono'})
            self.fields['telefono'].required=False
            self.fields['organizacion'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'organizacion'})
            self.fields['organizacion'].required=False


class EventoForm(forms.ModelForm):

    def file_size(value):  # add this to some file where you can import it from
        limit = 20 * 1024 * 1024
        if value.size > limit:
            raise ValidationError('Archivo demasiado grande. \
                                  El tamaño no debe superar 10 MB.')

    #certificado = forms.FileField(label='Seleccione una imagen SVG',
                             #help_text='max. 10 megabytes',
                             #validators=[file_size])
    class Meta:
        model = Evento
        fields = ('__all__')

    def __init__(self, *args, **kwargs):
        super(EventoForm, self).__init__(*args, **kwargs)
        self.fields['nombre_evento'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Nombre del evento'})
        self.fields['nombre_evento'].required=True
        self.fields['fecha_inicio'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Fecha inicio', 'data-provide':'datepicker'})
        self.fields['fecha_inicio'].required=True
        self.fields['fecha_fin'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Fecha fin', 'data-provide':'datepicker'})
        self.fields['fecha_fin'].required=True
        self.fields['certificado'].widget.attrs.update({'class':'form-control',
                                                   'data-show-preview':'true',
                                                   'accept':'image/svg'})
        self.fields['certificado'].required = True
        self.fields['activo'].required=True
        self.fields['activo'].label="Estara activo?"


class AddPartEventForm(forms.ModelForm):
    class Meta:
        model = ParticipanteEvento
        exclude = ['certificado']
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "El participante ya se encuentra registrado en el evento.",
            }
        }

    def __init__(self, *args, **kwargs):
        super(AddPartEventForm, self).__init__(*args, **kwargs)
        self.fields['fk_participante'].empty_label = 'Seleccione Participante'
        self.fields['fk_participante'].widget.attrs.update({'class': 'form-control'})
        self.fields['fk_participante'].required=True
        self.fields['fk_participante'].label='Participante'
        self.fields['fk_modalidad'].empty_label = 'Seleccione Modalidad en la que participa'
        self.fields['fk_modalidad'].widget.attrs.update({'class': 'form-control'})
        self.fields['fk_modalidad'].required=True
        self.fields['fk_modalidad'].label='Modalidad en la que participa'
        self.fields['fk_evento'].empty_label = 'Seleccione el Evento en que participa'
        self.fields['fk_evento'].widget.attrs.update({'class': 'form-control'})
        self.fields['fk_evento'].required=True
        self.fields['fk_evento'].label='Nombre del evento'


class ConsultarEventForm(forms.Form):
    evento = forms.CharField()

    def __init__(self, *args, **kwargs):
        eventos = obtenerAllEvento()
        super(ConsultarEventForm, self).__init__(*args, **kwargs)
        self.fields['evento'].widget = forms.Select(choices=[("", "Buscar por evento")] +
            [(even["pk"], even["nombre_evento"]) for even in eventos])
        self.fields['evento'].widget.attrs.update({'class': 'form-control'})
        self.fields['evento'].required = True
        self.fields['evento'].label = 'Listar los participantes del evento'


class UploadFileForm(forms.Form):
    """
    Clase que contiene el formulario para subir un archivo para la carga de historico
    """
    def file_size(value):  # add this to some file where you can import it from
        limit = 20 * 1024 * 1024
        if value.size > limit:
            raise ValidationError('Archivo demasiado grande. \
                                  El tamaño no debe superar10 MB.')
    evento = forms.CharField()
    cargar_mass = forms.FileField(label='Seleccione un archivo',
                             help_text='max. 10 megabytes',
                             validators=[file_size])

    def __init__(self, *args, **kwargs):
        eventos = obtenerAllEvento()
        super(UploadFileForm, self).__init__(*args, **kwargs)
        self.fields['evento'].widget = forms.Select(choices=[("", "Seleccione el evento:")] +
            [(even["pk"], even["nombre_evento"]) for even in eventos])
        self.fields['evento'].widget.attrs.update({'class': 'form-control'})
        self.fields['evento'].required = True
        self.fields['evento'].label = 'Registros que pertenecen al Evento:'
        self.fields['cargar_mass'].widget.attrs.update({'class':'form-control',
                                                   'data-show-preview':'true'})
        self.fields['cargar_mass'].required = True

    def clean(self):
        cleaned_data = super(UploadFileForm, self).clean()
        cargar_mass = cleaned_data.get("cargar_mass")

        if cargar_mass:
            msg = "Error en la extension del archivo debe ser un csv, ods o xls "
            extend = ['csv', 'xls', 'ods', 'xlsx']
            try:
                extension = str(cargar_mass).split('.')[1]
                if not(extension in extend):
                    self.add_error('cargar_mass', msg)
                else:
                    ruta = '%s/%s' % (settings.TMP, str(cargar_mass))
                    archivo = default_storage.save(ruta, ContentFile(cargar_mass.read()))
                    load_file = pyexcel.get_sheet(file_name=archivo)
                    if extension == "csv":
                        csv = True
                        control = load_file[0, 0].split(";")
                        cab_nombres = control[0].lower()
                        cab_apellidos = control[1].lower()
                        cab_ci = control[2].lower()
                        cab_correo = control[3].lower()
                        cab_telefono = control[4].lower()
                        cab_modalidad = control[5].lower()
                        cab_organizacion = control[6].lower()
                    else:
                        csv = False
                        cab_nombres = load_file[0, 0].lower()
                        cab_apellidos = load_file[0, 1].lower()
                        cab_ci = load_file[0, 2].lower()
                        cab_correo = load_file[0, 3].lower()
                        cab_telefono = load_file[0, 4].lower()
                        cab_modalidad = load_file[0, 5].lower()
                        cab_organizacion = load_file[0, 6].lower()
                    formato_cab = ['nombre', 'apellido', 'ci', 'correo',
                                   'telefono', 'modalidad', 'organizacion']
                    cabece_file = [cab_nombres, cab_apellidos, cab_ci,
                                   cab_correo, cab_telefono, cab_modalidad,
                                   cab_organizacion]
                    if cabece_file == formato_cab:
                        line = 2
                        for row in load_file.row[1:]:
                            almacen_error = ""
                            if csv:
                                row = row[0].split(';')
                            row_nombres = row[0].title().strip()
                            row_apellidos = row[1].title().strip()
                            row_ci = row[2]
                            row_correo = row[3].strip()
                            row_telefono = row[4]
                            row_modalidad = row[5].lower().strip()

                            valida_nom = validarString(row_nombres)
                            valida_appe = validarString(row_apellidos)
                            valida_ci = validarDigit(row_ci)
                            valida_correo = validarCorreo(str(row_correo))
                            valida_telefono = validarDigit(row_telefono)
                            valida_modalidad = validarModalidad(row_modalidad)
                            if valida_nom:
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s \n" % (cab_nombres, str(line))

                            if valida_appe:
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s " % (cab_apellidos, str(line))
                            if valida_ci:
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s " % (cab_ci, str(line))
                            if not(valida_correo):
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s " % (cab_correo, str(line))
                            if valida_telefono:
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s " % (cab_telefono, str(line))
                            if valida_modalidad:
                                almacen_error += "Error en la columna %s de \
                                                  la linea: %s : el campo debe ser alfabetico y tener algunos de estos valores: particiapante, ponente u organizador si el usuario participa en varias modalidades separalos de la siguiente manera:  ponente/organizador \n" % (cab_modalidad, str(line))
                            if almacen_error !="":
                                msg = almacen_error
                                self.add_error('cargar_mass', msg)
                            line = line + 1
                    else:
                        msg = "La cabecera debe tener el siguiente formato:\
                        nombre, apellido, ci, correo, telefono, modalidad, \
                        organizacion"
                        # elimina el archivo si fue creado en la carpeta tmp
                        os.unlink(ruta)
            except:
                # elimina el archivo si fue creado en la carpeta tmp
                os.unlink(ruta)
                self.add_error('cargar_mass', msg)


class UpdateEventoForm(forms.ModelForm):

    def file_size(value):  # add this to some file where you can import it from
        limit = 20 * 1024 * 1024
        if value.size > limit:
            raise ValidationError('Archivo demasiado grande. \
                                  El tamaño no debe superar 10 MB.')

    #certificado = forms.FileField(label='Seleccione una imagen SVG',
                             #help_text='max. 10 megabytes',
                             #validators=[file_size])
    class Meta:
        model = Evento
        exclude = ['nombre_evento']

    def __init__(self, *args, **kwargs):
        super(UpdateEventoForm, self).__init__(*args, **kwargs)
        self.fields['fecha_inicio'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Fecha inicio', 'data-provide':'datepicker'})
        self.fields['fecha_inicio'].required=True
        self.fields['fecha_fin'].widget.attrs.update({'class': 'form-control',
            'placeholder': 'Fecha fin', 'data-provide':'datepicker'})
        self.fields['fecha_fin'].required=True
        self.fields['certificado'].widget.attrs.update({'class':'form-control',
                                                   'data-show-preview':'true', 
                                                   'accept':'image/svg'})
        self.fields['certificado'].required = True
        self.fields['activo'].required=True
        self.fields['activo'].label="Estara activo?"