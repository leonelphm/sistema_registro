from django.conf import settings
from django.contrib import messages
from django_datatables_view.base_datatable_view import BaseDatatableView

from users.utils import LoginRequeridoPerAuth
from .forms import *
from .models import *


class ListPartEvetAjaxView(LoginRequeridoPerAuth, BaseDatatableView):
    """!
    Prepara la data para mostrar en el datatable

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 28-04-2017
    @version 1.0.0
    """
    # The model we're going to show
    model = ParticipanteEvento
    # define the columns that will be returned
    columns = ['fk_evento', 'fk_participante', 'fk_modalidad']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['fk_evento', 'fk_participante', 'fk_modalidad']
    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListPartEvetAjaxView, self).__init__()

    def get_initial_queryset(self):
        """!
        Consulta el modelo ParticipanteEvento

        @return: Objeto de la consulta
        """
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        try:
            evento = self.request.GET['evento']
        except:
            return self.model.objects.select_related().all()
        if evento != 'None':
            return self.model.objects.select_related().all().filter(fk_evento=evento)

    def prepare_results(self, qs):
        """!
        Prepara la data para mostrar en el datatable
        @return: Objeto json con los datos de los usuarios
        """
        # prepare list with output column data
        json_data = []
        for item in qs:
            evento = "<a  class='btn btn-block btn-info btn-xs fa fa-plus'> %s</a>" % (str(item.fk_evento))
            json_data.append([
            evento,
            "{0} {1}".format(str(item.fk_participante.nombres), str(item.fk_participante.apellidos)),
            str(item.fk_participante.cedula),
            str(item.fk_participante.correo),
            str(item.fk_modalidad)
            ])
        return json_data


class ListAjaxEventsView(LoginRequeridoPerAuth, BaseDatatableView):
    """!
    Prepara la data para mostrar en el datatable

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 28-04-2017
    @version 1.0.0
    """
    # The model we're going to show
    model = Evento
    # define the columns that will be returned
    columns = ['nombre_evento', 'fecha_inicio', 'fecha_fin', 'activo']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['nombre_evento', 'fecha_inicio', 'fecha_fin', 'activo']
    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListAjaxEventsView, self).__init__()

    def get_initial_queryset(self):
        """!
        Consulta el modelo Evento

        @return: Objeto de la consulta
        """
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return self.model.objects.all()

    def prepare_results(self, qs):
        """!
        Prepara la data para mostrar en el datatable
        @return: Objeto json con los datos de los usuarios
        """
        # prepare list with output column data
        json_data = []
        for item in qs:
            if item.activo:
                evento = "<a data-toggle='modal' data-target='#myModal' class='btn btn-block btn-info btn-xs fa fa-edit' onclick='modal_events(%s)'>%s</a>" % (str(item.pk), str(item.nombre_evento))
                activo = "Activo"
                activar = "<input type='checkbox' id='user-" + str(item.pk) + "' value='" + str(item.pk) + "' name='inactivar' onclick='$(\"#forma_activar\").submit();'/>\
                            <label for='user-" + str(item.pk) + "'>\
                                <img  src='" + settings.MEDIA_URL + \
                                "imagenes/inactivar.png' id='inactivo' \
                                title='Inactivar Evento' \
                                />\
                            </label>"
            else:
                evento = str(item.nombre_evento)
                activo = "Inactivo"
                activar = "<input type='checkbox' id='user-" + str(item.pk) + "' value='" + str(item.pk) + "' name='activar' onclick='$(\"#forma_activar\").submit();'/>\
                            <label for='user-" + str(item.pk) + "'>\
                                <img  src='" + settings.MEDIA_URL + \
                                "imagenes/activar.png' id='activo' \
                                title='Activar Evento'/>\
                            </label>"
            json_data.append([
            evento,
            item.fecha_inicio.strftime("%d-%m-%Y"),
            item.fecha_fin.strftime("%d-%m-%Y"),
            activo,
            activar
            ])
        return json_data


class ListAjaxPariticipantesView(LoginRequeridoPerAuth, BaseDatatableView):
    """!
    Prepara la data para mostrar en el datatable

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 28-04-2017
    @version 1.0.0
    """
    # The model we're going to show
    model = Participante
    # define the columns that will be returned
    columns = ['nombres', 'apellidos', 'cedula', 
               'correo', 'telefono', 'organizacion']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['nombres', 'apellidos', 'cedula', 
                     'correo', 'telefono', 'organizacion']
    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListAjaxPariticipantesView, self).__init__()

    def get_initial_queryset(self):
        """!
        Consulta el modelo Evento

        @return: Objeto de la consulta
        """
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return self.model.objects.all()

    def prepare_results(self, qs):
        """!
        Prepara la data para mostrar en el datatable
        @return: Objeto json con los datos de los usuarios
        """
        # prepare list with output column data
        json_data = []
        for item in qs:
            cedula = "<a  class='btn btn-block btn-info btn-xs fa fa-edit'>%s</a>" % (str(item.cedula))
            json_data.append([
            "{0} {1}".format(str(item.nombres), str(item.apellidos)),
            cedula,
            item.correo,
            item.telefono,
            item.organizacion
            ])
        return json_data


class ListByCertAjaxView(LoginRequeridoPerAuth, BaseDatatableView):
    """!
    Prepara la data para mostrar en el datatable

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 28-04-2017
    @version 1.0.0
    """
    # The model we're going to show
    model = ParticipanteEvento
    # define the columns that will be returned
    columns = ['fk_evento', 'fk_participante', 'fk_modalidad', 'certificado']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['fk_evento', 'fk_participante',
                     'fk_modalidad', 'certificado']
    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    permission_required = 'auth.change_user'

    def __init__(self):
        super(ListByCertAjaxView, self).__init__()

    def get_initial_queryset(self):
        """!
        Consulta el modelo ParticipanteEvento

        @return: Objeto de la consulta
        """
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        try:
            evento = self.request.GET['evento']
        except:
            return self.model.objects.select_related().all()
        if evento != 'None':
            return self.model.objects.select_related().all().filter(fk_evento=evento)

    def prepare_results(self, qs):
        """!
        Prepara la data para mostrar en el datatable
        @return: Objeto json con los datos de los usuarios
        """
        # prepare list with output column data
        json_data = []
        for item in qs:
            if item.certificado == "":
                certifico = "No esta Certificado"
                certificar = "<input type='checkbox' id='user-" + str(item.pk) + "' value='" + str(item.pk) + "' name='certificar' onclick='$(\"#forma_certificar\").submit();'/>\
                            <label for='user-" + str(item.pk) + "'>\
                                <img  src='" + settings.MEDIA_URL + \
                                "imagenes/activar.png' id='certificar' \
                                title='Certificar' \
                                />\
                            </label>"
            else:
                certifico = "Ya esta Certificado <a target='_blank' href='%s' class='btn btn-block btn-info btn-xs fa fa-eye'> %s</a>" % (settings.MEDIA_URL+str(item.certificado), str(item.fk_evento))
                certificar = "----"
            evento = str(item.fk_evento)
            json_data.append([
            evento,
            "{0} {1}".format(str(item.fk_participante.nombres), str(item.fk_participante.apellidos)),
            str(item.fk_participante.cedula),
            str(item.fk_participante.correo),
            str(item.fk_modalidad),
            certifico,
            certificar
            ])
        return json_data
