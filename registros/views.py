import os
from PyPDF2 import PdfFileMerger

from django.conf import settings
from django.shortcuts import (
    render, redirect
)
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic.edit import (
    FormView, UpdateView
)
from django.views.generic import TemplateView
from django.core.urlresolvers import (
    reverse_lazy, reverse
)

from users.utils import LoginRequeridoPerAuth
from subprocess import Popen
from .forms import *
from .models import *


class RegisterView(LoginRequeridoPerAuth, FormView):
    """!
    Muestra el formulario de registro de participantes

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "participantes/register.html"
    form_class = ParticiapanteForm
    success_url = reverse_lazy('registro:registrar_participante')
    model = Participante
    model_event = Evento
    model_modal = ModalidadParticipacion
    model_rela = ParticipanteEvento
    permission_required = 'auth.add_user'


    def form_valid(self, form, **kwargs):
        """
        Valida el formulario de registro de participantes
        @return: Dirige con un mensaje de exito a la misma pagina
        """
        formulario = self.form_class(self.request.POST)
        nuevo_participante = formulario.save()
        modalidad = form.cleaned_data['fk_modalidad']
        evento = form.cleaned_data['fk_evento']
        evento = self.model_event.objects.get(pk=evento)
        modalidad = self.model_modal.objects.get(pk=modalidad)
        agregar_participante = self.model_rela(fk_evento=evento,
                                               fk_modalidad=modalidad,
                                               fk_participante=nuevo_participante)
        agregar_participante.save()
        messages.success(self.request, "El usuario %s creo con exito el participante:\
                                        %s" % (str(self.request.user), nuevo_participante.cedula))
        return super(RegisterView, self).form_valid(form)


class RegisterEventView(LoginRequeridoPerAuth, FormView):
    """!
    Muestra el formulario de registro de usuarios

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "eventos/register.html"
    form_class = EventoForm
    success_url = reverse_lazy('registro:registrar_evento')
    model = Evento
    permission_required = 'auth.add_user'

    def form_valid(self, form, **kwargs):
        """
        Valida el formulario de registro de eventos
        @return: Dirige con un mensaje de exito a la misma pagina
        """
        formulario = self.form_class(self.request.POST, self.request.FILES)
        nuevo_envento = formulario.save()
        messages.success(self.request, "El usuario %s creo con exito el evento:\
                                        %s" % (str(self.request.user),
                                               nuevo_envento.nombre_evento))
        return super(RegisterEventView, self).form_valid(form)


class AddPartEventView(LoginRequeridoPerAuth, FormView):
    """!
    Muestra el formulario de registro para asociar los participante a un evento.

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "participantes/add_participante_evento.html"
    form_class = AddPartEventForm
    success_url = reverse_lazy('registro:add_participante_evento')
    permission_required = 'auth.add_user'


    def form_valid(self, form, **kwargs):
        """
        Valida el formulario de registro de participantes a un evento
        @return: Dirige con un mensaje de exito a la misma pagina
        """
        formulario = self.form_class(self.request.POST)
        agregar_participante = formulario.save()
        messages.success(self.request, "El usuario %s agrego al participante: %s \
                                        con exito el evento: %s " % (
                                            str(self.request.user),
                                            agregar_participante.fk_participante,
                                            agregar_participante.fk_evento))
        return super(AddPartEventView, self).form_valid(form)

    def form_invalid(self, form, **kwargs):
        messages.error(self.request, form.errors)
        return super(AddPartEventView, self).form_valid(form)


class ListEventsPartView(LoginRequeridoPerAuth, FormView):
    """!
    Listar Eventos y participantes de la aplicacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "eventos/list_event.html"
    model = ParticipanteEvento
    form_class = ConsultarEventForm
    success_url = reverse_lazy('registro:list_participante_evento')
    permission_required = 'auth.change_user'


class ListEventsView(LoginRequeridoPerAuth, TemplateView):
    """!
    Listar Eventos de la aplicacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "eventos/list_eventos.html"
    model = Evento
    success_url = reverse_lazy('registro:list_events')
    permission_required = 'auth.change_user'

    def post(self, *args, **kwargs):
        '''
        Cambia el estado activo o inactivo a el evento
        @return: Dirige a la tabla que muestra los eventos de la apliacion
        '''
        accion = self.request.POST
        activar = accion.get('activar', None)
        inactivar = accion.get('inactivar', None)
        estado = False

        if activar is not None:
            user = activar
            estado = True
        elif inactivar is not None:
            user = inactivar
            estado = False
        else:
            messages.error(self.request, "Esta intentando hacer una accion incorrecta")      
        try:
            evento_act = self.model.objects.get(pk=user)
            evento_act.activo = estado
            evento_act.save()
            if estado:
                messages.success(self.request, "Se activo el evento: %s" % (str(evento_act)))
            else:
                messages.warning(self.request, "Se ha inactivado el evento: %s" % (str(evento_act)))
        except:
            messages.info(self.request, "El evento no existe")
        return redirect(self.success_url)


class ListParticipantesView(LoginRequeridoPerAuth, TemplateView):
    """!
    Listar Eventos de la aplicacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "participantes/list_participantes.html"
    model = Participante
    success_url = reverse_lazy('registro:list_participantes')
    permission_required = 'auth.change_user'


class UploadRegistroView(LoginRequeridoPerAuth, FormView):
    """!
    Listar Eventos y participantes de la aplicacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "participantes/upload_register.html"
    form_class = UploadFileForm
    success_url = reverse_lazy('registro:upload_register')
    permission_required = 'auth.change_user'
    model = Participante
    model_event = Evento

    def form_valid(self, form, **kwargs):
        """
        Valida el formulario de registro de participantes a un evento
        @return: Dirige con un mensaje de exito a la misma pagina
        """
        cargar_mass = form.cleaned_data.get("cargar_mass")
        evento = form.cleaned_data.get("evento")
        evento = self.model_event.objects.get(pk=evento)
        ruta = '%s/%s' % (settings.TMP, str(cargar_mass))
        archivo = ruta

        instance = self.model()
        extension = str(cargar_mass).split('.')[1]
        process = instance.uploadCreateUpdate(archivo, self.request.user,
                                              extension, evento)
        # elimina el archivo después de cargar los datos
        os.unlink(ruta)
        messages.success(self.request, "El usuario agrego la carga masiva \
                                        con existo al evento: %s" % (evento))
        return super(UploadRegistroView, self).form_valid(form)

    def form_invalid(self, form, **kwargs):
        messages.error(self.request, form.errors)
        return super(UploadRegistroView, self).form_valid(form)


class EventUpdateModals(LoginRequeridoPerAuth, FormView):
    """!
    Construye el modals para la actualizacion del evento

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    form_class = UpdateEventoForm
    model = Evento
    template_name = "eventos/modals_updateEvent.html"
    success_url = reverse_lazy('registro:list_events')
    permission_required = 'auth.change_user'

    def get_context_data(self, **kwargs):
        """!
        Carga el formulario en la vista,para actualizar el perfil del usuario
        @return: El contexto con los objectos para la vista
        """
        evento = self.request.GET.get('id_evento', None)
        try:
            evento = self.model.objects.get(pk=evento)
        except:
            evento = None
            messages.error(self.request, "El Evento que intenta \
                                          actualizar no existe.")
            return super(EventUpdateModals, self).get_context_data(**kwargs)
        form = self.form_class(instance=evento)
        context = super(EventUpdateModals, self).get_context_data(**kwargs)
        context['evento'] = evento
        context['form'] = form
        return context

    def form_valid(self, form, **kwargs):
        pk_evento = self.request.POST.get('evento')
        try:
            evento = self.model.objects.filter(activo=True).get(pk=pk_evento)
            formulario = self.form_class(self.request.POST,
                                         self.request.FILES, instance=evento)
            formulario.save()
            messages.success(self.request, "Evento %s Actualizado con exito\
                                           " % (str(evento)))
        except:
            messages.error(self.request, "Evento %s no se pudo actualizar \
                                           #" % (str(evento)))

        return super(EventUpdateModals, self).form_valid(form)


class CertifByPartView(LoginRequeridoPerAuth, FormView):
    """!
    Listar Eventos y participantes de la aplicacion

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    template_name = "certificaciones/certificacion_participante.html"
    form_class = ConsultarEventForm
    success_url = reverse_lazy('registro:certificar_participante')
    permission_required = 'auth.change_user'
    model = ParticipanteEvento

    def post(self, *args, **kwargs):
        certificado = self.request.POST.get('certificar')
        merger = PdfFileMerger()
        try:
            if certificado:
                consulta = self.model.objects.select_related().get(pk=certificado)
                messages.success(self.request, "Paritcipante %s certificado con exito\
                                            en el evento: %s" % (str(consulta.fk_participante), str(consulta.fk_evento)))
            else:
                messages.error(self.request, "Participante no se puede certificar")
                return redirect(self.success_url)
            reemplazos = {'Nombre_Participante': str(consulta.fk_participante.nombres) + " " + str(consulta.fk_participante.apellidos),
                         'cedula': str(consulta.fk_participante.cedula),
                         'Rol': 'Por su participación como <tspan font-weight = "bold" font-style = "italic">' + str(consulta.fk_modalidad) + '</tspan>',
                         'Evento': str(consulta.fk_evento),
                         'subtitulo': "",
                         'Fecha': 'Mérida'+consulta.fk_evento.fecha_inicio.strftime("%d-%m-%Y")}
            ruta_cert_base = settings.SOURCES_URL + str(consulta.fk_evento.certificado)
            certificado_ruta = str(consulta.fk_evento.certificado).split("/")
            ruta_certificado = certificado_ruta[0] + "/" + certificado_ruta[1] + "/" + str(consulta.fk_participante.cedula)
            if os.path.exists(settings.SOURCES_URL+ruta_certificado):
                nombretmp = settings.SOURCES_URL + ruta_certificado + "/certificado.svg"
                ruta_pdf = settings.SOURCES_URL + ruta_certificado + '/' + str(consulta.fk_participante.cedula) + "-" + str(consulta.fk_evento) + '.pdf'
            else:
               os.mkdir(settings.SOURCES_URL + ruta_certificado)
               nombretmp = settings.SOURCES_URL + ruta_certificado + "/certificado.svg"
               ruta_pdf = settings.SOURCES_URL + ruta_certificado + '/' + str(consulta.fk_participante.cedula) + "-" + str(consulta.fk_evento) + '.pdf'
            with open(ruta_cert_base, 'rb') as certificado_base, open(nombretmp, 'wb') as salida:
                for line in certificado_base: #Reemplazo de variables en el archivo svg
                    for src, target in reemplazos.items():
                        line = line.replace(src.encode("utf-8"), target.encode("utf-8"))
                    salida.write(line)
            certificado_base.close()
            salida.close()
            certificado_pdf = ruta_pdf
            save_pdf = ruta_certificado + '/' + str(consulta.fk_participante.cedula) + "-" + str(consulta.fk_evento) + '.pdf'
            x = Popen(['/usr/bin/inkscape', nombretmp, '-A', certificado_pdf])
            consulta.certificado = save_pdf
            consulta.save()
        except:
            messages.error(self.request, "El participante no se encuentra regisrado en el evento")
            return redirect(self.success_url)
        return redirect(self.success_url)
