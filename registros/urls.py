# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import *
from .ajax import *

urlpatterns_registro = [
    #  Views Controllers
    url(r'^registrar-participantes/$', RegisterView.as_view(), name="registrar_participante"),
    url(r'^registrar-eventos/$', RegisterEventView.as_view(), name="registrar_evento"),
    url(r'^add-participante-eventos/$', AddPartEventView.as_view(), name="add_participante_evento"),
    url(r'^list-participante-evento/$', ListEventsPartView.as_view(), name="list_participante_evento"),
    url(r'^list-events/$', ListEventsView.as_view(), name="list_events"),
    url(r'^list-participantes/$', ListParticipantesView.as_view(), name="list_participantes"),
    url(r'^upload-register/$', UploadRegistroView.as_view(), name="upload_register"),
    url(r'^update-event/$', EventUpdateModals.as_view(), name="update_event"),
    url(r'^certificar-participante/$', CertifByPartView.as_view(), name="certificar_participante"),

    #  Ajax Controllers
    url(r'^list-ajax-partic-event/$', ListPartEvetAjaxView.as_view(), name="list_ajax_partic_event"),
    url(r'^list-ajax-events/$', ListAjaxEventsView.as_view(), name="list_ajax_events"),
    url(r'^list-ajax-participantes/$', ListAjaxPariticipantesView.as_view(), name="list_ajax_participantes"),
    url(r'^ajax-certificacion-participante/$', ListByCertAjaxView.as_view(), name="ajax_certificacion_participante"),
]

