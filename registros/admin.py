from django.contrib import admin
from .models import ParticipanteEvento
from .forms import *
# Register your models here.

class ParticipanteEventoAdmin(admin.ModelAdmin):
    form = AddPartEventForm

admin.site.register(ParticipanteEvento, ParticipanteEventoAdmin)
