(function($) {  
    $.get = function(key)   {  
        key = key.replace(/[\[]/, '\\[');  
        key = key.replace(/[\]]/, '\\]');  
        var pattern = "[\\?&]" + key + "=([^&#]*)";  
        var regex = new RegExp(pattern);  
        var url = unescape(window.location.href);  
        var results = regex.exec(url);  
        if (results === null) {  
            return null;  
        } else {  
            return results[1];  
        }  
    }
})(jQuery);

$(document).ready(function (){
        $('#id_fecha_inicio').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            autoSize: true,
            maxDate: 0,
            onClose: function( selectedDate ) {
                if(selectedDate)
                {
                    $( "#id_fecha_fin" ).datepicker("option", "minDate", selectedDate );
                }
                else
                {
                    $( "#id_fecha_fin" ).datepicker("option", "minDate", 0 );
                }
            }
        });
        $('#id_fecha_fin').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            autoSize: true,
            maxDate: 0,
            onClose: function( selectedDate ) {
                if(selectedDate)
                {
                    $( "#id_fecha_inicio" ).datepicker( "option", "maxDate", selectedDate );
                }
                else
                {
                    $( "#id_fecha_inicio" ).datepicker( "option", "maxDate", 0 );
                }
            }
        });
});