$(document).ready(function() {
   $('#datatable').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": LISTAR,
        /*"dom": 'Bfrtip',
        "buttons": [
            'csv', 'pdf'
        ],*/
        language: {
            url: JSON_DATA
        }
        });
    $('#datatable')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
});