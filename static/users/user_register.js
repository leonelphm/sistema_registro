function cargar_permisos(id_permisos){
  $.ajax({
    url: CARGAR_PERMISOS,
    type: "GET",
    data: {
        groups: id_permisos
    },
    dataType: 'html',
    success: function(data) {
              $("#permisos_form").show()
              $('#permisos_form').html(data);
              if ($('#id_is_staff').is(':checked')){
                $.each([3], function(i,e){
                $("#id_groups option[value='" + e + "']").prop("selected", true);
                });
                $("#id_user_permissions").attr('readonly', '')
                $("#id_groups").attr('readonly', '')
              }
              $('#id_user_permissions option').each(function(i){ 
                    $("#id_user_permissions option[value='" + i + "']").prop("selected", true); 
                  });
          }

  });
}
$(document).ready(function() {
    $("#id_is_staff").on("ifChanged", function(){
        if ($('#id_is_staff').is(':checked')){
            cargar_permisos([3,])
        }
        else{
        $("#id_groups").removeAttr('readonly')
        $("#id_user_permissions").removeAttr('readonly')
        $("#permisos_form").hide()
         $.each([3], function(i,e){
            $("#id_groups option[value='" + e + "']").prop("selected", false);
          });
         $('#id_user_permissions option').each(function(i){ 
              $("#id_user_permissions option[value='" + i + "']").prop("selected", false); 
            });
        }
    })
    $('#id_groups').change(function() {
       cargar_permisos($(this).val())
    });
});
