$(document).ready(function() {
   $('#datatable').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": LISTAR_USER,
        language: {
            url: JSON_DATA
        },
        /*dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],*/
        });
    $('#datatable')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
});