# -*- coding: utf-8 -*-
from dal import autocomplete
from django.core.exceptions import ValidationError
from django import forms

from .models import (
    Pais, Estado, Municipio, Parroquia
)



class UploadFileForm(forms.Form):
    """
    Clase que contiene el formulario para subir un archivo para la carga de historico
    """
    def file_size(value): # add this to some file where you can import it from
        limit = 20 * 1024 * 1024
        if value.size > limit:
            raise ValidationError('Archivo demasiado grande. El tamaño no debe superar10 MB.')
    cargar = forms.FileField(label='Seleccione un archivo',
                             help_text='max. 10 megabytes',
                             validators=[file_size])


class EstadoForm(forms.ModelForm):
    pais = forms.ModelChoiceField(
    queryset=Pais.objects.all(),
    widget=autocomplete.ModelSelect2(url='utils:pais_autocomplete')
    )

    class Meta:
        model = Estado
        fields = ('__all__')


class MunicipioForm(forms.ModelForm):
    estado = forms.ModelChoiceField(
    queryset=Estado.objects.all(),
    widget=autocomplete.ModelSelect2(url='utils:estado_autocomplete')
    )

    class Meta:
        model = Municipio
        fields = ('__all__')


class ParroquiaForm(forms.ModelForm):
    municipio = forms.ModelChoiceField(
    queryset=Municipio.objects.all(),
    widget=autocomplete.ModelSelect2(url='utils:municipio_autocomplete')
    )

    class Meta:
        model = Parroquia
        fields = ('__all__')
