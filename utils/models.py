from django.db import models


class Pais(models.Model):
    """!
    Clase que contiene los países

    @author Ing. Leonel P Hernandez M (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18/07/2017
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre



class Estado(models.Model):
    """!
    Clase que contiene los Estados de un Pais

    @author Ing. Leonel P Hernandez M (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18/07/2017
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    pais = models.ForeignKey(Pais)


    def __str__(self):
        return self.nombre


class Municipio(models.Model):
    """!
    Clase que contiene los Municipios de un Estado

    @author Ing. Leonel P Hernandez M (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18/07/2017
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    estado = models.ForeignKey(Estado)

    def __str__(self):
        return self.nombre


class Parroquia(models.Model):
    """!
    Clase que contiene las parroquias de un Municipio

    @author Ing. Leonel P Hernandez M (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 18/07/2017
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    municipio = models.ForeignKey(Municipio)

    def __str__(self):

        return self.nombre
