# -*- coding: utf-8 -*-

MENSAJES_LOGIN = {
    'LOGIN_USUARIO_NO_VALIDO': 'El usuario no es válido',
    'LOGIN_REQUERIDO': 'Necesita loguearse para acceder al sistema.'
}