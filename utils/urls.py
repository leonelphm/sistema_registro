# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import *

urlpatterns_autocomplete = [
    url(r'^pais-autocomplete/$', PaisAutocomplete.as_view(),
    name='pais_autocomplete',
    ),
    url(r'^estado-autocomplete/$', EstadoAutocomplete.as_view(),
    name='estado_autocomplete',
    ),
    url(r'^municipio-autocomplete/$', MunicipioAutocomplete.as_view(),
    name='municipio_autocomplete',
    ),
    url(r'^parroquia-autocomplete/$', ParroquiaAutocomplete.as_view(),
    name='parroquia_autocomplete',
    ),
    url(r'^participante-autocomplete/$', ParticipanteAutocomplete.as_view(),
    name='participante_autocomplete',
    ),
]
