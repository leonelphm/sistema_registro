# -*- encoding: utf-8 -*-
"""!
Vista que contraola los procesos de las utilidades de la plataforma

@author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
@date 09-01-2017
@version 1.0.0
"""

from django.shortcuts import render
from dal import autocomplete

from .models import *

from registros.models import (
    Participante
)


class PaisAutocomplete(autocomplete.Select2QuerySetView):
    """!
    Crea el autocomplete para los Paises

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    def get_queryset(self):
        # No se olvide de filtrar los resultados en función del visitante !
        if not self.request.user.is_authenticated():
            return Pais.objects.none()

        qs = Pais.objects.all()
        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class EstadoAutocomplete(autocomplete.Select2QuerySetView):
    """!
    Crea el autocomplete para los Estados

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """

    def get_queryset(self):
        # No se olvide de filtrar los resultados en función del visitante !
        if not self.request.user.is_authenticated():
            return Estado.objects.none()

        qs = Estado.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    """!
    Crea el autocomplete para los Municipios

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """

    def get_queryset(self):
        # No se olvide de filtrar los resultados en función del visitante !
        if not self.request.user.is_authenticated():
            return Municipio.objects.none()

        qs = Municipio.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class ParroquiaAutocomplete(autocomplete.Select2QuerySetView):
    """!
    Crea el autocomplete para las Parroquias

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """

    def get_queryset(self):
        # No se olvide de filtrar los resultados en función del visitante !
        if not self.request.user.is_authenticated():
            return Parroquia.objects.none()

        qs = Parroquia.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class ParticipanteAutocomplete(autocomplete.Select2QuerySetView):
    model = Participante
    """!
    Crea el autocomplete de los Participantes

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    def get_queryset(self):
        # No se olvide de filtrar los resultados en función del visitante !
        if not self.request.user.is_authenticated():
            return self.model.objects.none()

        qs = self.model.objects.all()
        if self.q:
            qs = qs.filter(cedula__istartswith=self.q)

        return qs

    def get_result_label(self, item):
        return '<img src="%s"> %s' % (item.pk, item.cedula)

